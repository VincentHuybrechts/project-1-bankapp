package be.multimedi.project1.thebigbank.helper;


import java.util.Scanner;

public class KeyboardUtility {

    private static final Scanner INPUT = new Scanner(System.in);

    public static int askForInt(String message) {
        System.out.print( message);
        int userInput = INPUT.nextInt();
        INPUT.nextLine(); //automatically clear buffer after input to avoid buffer problem
        return userInput;
    }

    public static double askForDouble(String message) {
        System.out.print( message);
        double userInput = INPUT.nextDouble();
        INPUT.nextLine(); //automatically clear buffer after input to avoid buffer problem
        return userInput;
    }

    public static String askForText(String message) {
        System.out.print(message);
        return INPUT.nextLine();
    }

    public static void shutdown() {
        INPUT.close();
    } // shutdown not used yet. don't know how to implement without error

}
