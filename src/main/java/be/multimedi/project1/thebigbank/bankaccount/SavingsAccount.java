package be.multimedi.project1.thebigbank.bankaccount;

public class SavingsAccount extends BankAccount {

    public SavingsAccount(int clientNumber, String iban, int pinCode, double balance) {
        super(clientNumber, iban, pinCode, balance, "Savings Account"); //When a new object is made, the variable "type" in the superclass
                                                                            // is automatically filled in as a current account type
    }

}
