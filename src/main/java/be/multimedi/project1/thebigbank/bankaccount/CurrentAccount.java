package be.multimedi.project1.thebigbank.bankaccount;

public class CurrentAccount extends BankAccount {

    public CurrentAccount(int clientNumber, String iban, int pinCode, double balance) {
        super(clientNumber, iban, pinCode, balance , "CurrentAccount");  //When a new object is made, the variable "type" in the superclass
                                                                            // is automatically filled in as a current account type
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
