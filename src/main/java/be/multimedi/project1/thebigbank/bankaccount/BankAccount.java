package be.multimedi.project1.thebigbank.bankaccount;

public abstract class BankAccount {
    // Final because this information is not changeable, we don't provide an option to change client details
    private final int clientNumber;
    private final String iban;
    private final int pinCode;
    private double balance;   // except for balance we need a setter, to change balances after transfers, so not final
    private final String type;

    public BankAccount(int clientNumber, String iban, int pinCode, double balance, String type) {
        this.clientNumber = clientNumber;
        this.iban = iban;
        this.pinCode = pinCode;
        this.balance = balance;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public int getClientNumber() {
        return clientNumber;
    }

    public String getIban() {
        return iban;
    }

    public int getPinCode() {
        return pinCode;
    }

    public double getBalance() {
        return balance;
    }

    // negative balances not allowed
    public void setBalance(double balance) {
        if (balance >= 0) {
            this.balance = balance;
        } else {
            System.out.printf("€%,.2f is not allowed, negative forbidden\n", balance);
        }

    }

    @Override
    public String toString() {
        return String.format("IBAN: %20s\nBalance: %,.2f", getIban(), getBalance());
    }
}

