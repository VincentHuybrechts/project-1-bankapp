package be.multimedi.project1.thebigbank.bankaccount;

import be.multimedi.project1.thebigbank.client.ClientProfile;
import be.multimedi.project1.thebigbank.transaction.Transaction;

public class Database {
//     DATABASE

    private static final BankAccount[] allBankAccounts = {
            new CurrentAccount(1, "BE123456789", 1234, 3_000.50),
            new SavingsAccount(1, "BE987654321", 4321, 50_000.83),
            new CurrentAccount(2, "BE963256789", 1234, 2_000.50),
            new SavingsAccount(2, "BE456254321", 4321, 52_000.83)
    };

    // Why client number? In order to make it possible to have several accounts linked to 1 profile.
    // This would make it possible to print an overview of all bank accounts linked to 1 profile, not necessary in this exercise.
    private static final ClientProfile[] clientProfiles = {
            new ClientProfile(1, "Vincent", "Huybrechts", showBankAccounts(ClientProfile.getClientNumber())),
            new ClientProfile(2, "Eva", "Xia", showBankAccounts(ClientProfile.getClientNumber()))
    };

//TODO: continuïteit wanneer we uitloggen  in de ontvanger profiel moet dat persoon ook bij incoming iets zien.
    private static Transaction[] incomingTransactions = {

    };

    public static Transaction[] outgoingTransactions = {

    };


    //METHODEN DATABASE
    private static BankAccount[] showBankAccounts(int clientNumber) {
        BankAccount[] accountsPerClient = new BankAccount[10];
        for (int i = 0; i < allBankAccounts.length; i++) {
            if (allBankAccounts[i].getClientNumber() == clientNumber) {
                accountsPerClient[i] = allBankAccounts[i];
            }
        }
        return accountsPerClient;
    }

    public static void addOutgoingTransfer(Transaction transaction) {

        // Temporary array with same length as original + 1
        Transaction[] newarr = new Transaction[outgoingTransactions.length + 1];

        // Preserve original data by copying it to the tempArray
        for (int i = 0; i < outgoingTransactions.length; i++) {
            newarr[i] = outgoingTransactions[i];
        }
        // Drop original array, and point to the new, larger array
        outgoingTransactions = newarr;

        // Now that there is finally 1 extra space, add new transfer
        outgoingTransactions[outgoingTransactions.length - 1] = transaction;

    }

    public static void addIncomingTransfer(Transaction transaction) {

        // Temporary array with same length as original + 1
        Transaction[] newarr = new Transaction[incomingTransactions.length + 1];

        // Preserve original data by copying it to the tempArray
        for (int i = 0; i < incomingTransactions.length; i++) {
            newarr[i] = incomingTransactions[i];
        }
        // Drop original array, and point to the new, larger array
        incomingTransactions = newarr;

        // Now that there is finally 1 extra space, add new transfer
        incomingTransactions[incomingTransactions.length - 1] = transaction;

    }

    public static BankAccount[] getAllBankAccounts() {
        return allBankAccounts;
    }

    public static ClientProfile[] getClientProfiles() {
        return clientProfiles;
    }

    public static Transaction[] getIncomingTransactions() {
        return incomingTransactions;
    }

    public static Transaction[] getOutgoingTransactions() {
        return outgoingTransactions;
    }

}
