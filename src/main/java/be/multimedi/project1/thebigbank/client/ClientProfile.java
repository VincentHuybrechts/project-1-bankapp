package be.multimedi.project1.thebigbank.client;

import be.multimedi.project1.thebigbank.bankaccount.BankAccount;

public class ClientProfile {
    private static int clientNumber;
    private String firstName;
    private String lastName;
    private BankAccount[] accountList;

    public ClientProfile(int clientNumber, String firstName, String lastName, BankAccount[] accountList) {
        this.clientNumber = clientNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountList = accountList;
    }

    public static int getClientNumber() {
        return clientNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BankAccount[] getAccountList() {
        return accountList;
    }

    public void setAccountList(BankAccount[] accountList) {
        this.accountList = accountList;
    }

    @Override
    public String toString() {
        return String.format("Firstname: %s\nLastname: %s\n", getFirstName() ,getLastName());
    }


}
