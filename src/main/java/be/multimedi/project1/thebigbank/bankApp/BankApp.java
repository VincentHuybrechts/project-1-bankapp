package be.multimedi.project1.thebigbank.bankApp;

import be.multimedi.project1.thebigbank.bankaccount.Database;
import be.multimedi.project1.thebigbank.client.ClientProfile;
import be.multimedi.project1.thebigbank.helper.KeyboardUtility;
import be.multimedi.project1.thebigbank.transaction.IncomingTransaction;
import be.multimedi.project1.thebigbank.transaction.OutgoingTransaction;
import be.multimedi.project1.thebigbank.transaction.Transaction;

import java.time.LocalDateTime;
import java.util.Locale;

public class BankApp {

    public static void main(String[] args) {
        showLoginScreen();
    }

    //METHODS______________________________________________________________
    private static String iban = null;
    private static String senderIban;


    //LOGIN SCREEN_________________________________________________________

    public static void showLoginScreen() {
        System.out.print("\t\t\t\tTHE BIG BANK-APP");
        starLine("\tWelcome! Please log in\n");
// toUppercase, more robust
        iban = KeyboardUtility.askForText("\tPlease enter your IBAN: ").toUpperCase(Locale.ROOT);

        while (isFoundInAllBankAccounts(iban)) {    // if iban is not found
            iban = KeyboardUtility.askForText("\tWrong IBAN, please enter your IBAN again: ").toUpperCase(Locale.ROOT);
        }
        System.out.println("\tvalid IBAN! ");

        // when iban is valid, enter pin
        int pin = KeyboardUtility.askForInt("\tPlease enter your pin: ");

        int count = 1;
        boolean notLoggedIn = true;
        while (count <= 3 && notLoggedIn) {      // 3 attempts
            if (!(isPinCodeCorrect(pin, iban))) {
                if (!(count == 3)) {
                    pin = KeyboardUtility.askForInt("\tWrong pin, please enter your pin again: (you have " + (3 - count) + " attempts left)");
                    count++;

                } else {
                    System.out.println();
                    System.out.println("\tYou have no more attempts left, please contact your branch");
                    System.out.println();
                    System.out.println("\tGoodbye, shutdown....");
                    showLoginScreen();
                }
            } else {
                notLoggedIn = false;
            }

        }
        System.out.println("\tCorrect pin!\n");
        System.out.print("\n");
        showMenuScreen(iban);
    }

    private static boolean isPinCodeCorrect(int pinInputFromUser, String iban) {
        boolean pinCodeDoesNotMatchIban = true;
        int pinCodeFromList = 0;
        for (int i = 0; i < Database.getAllBankAccounts().length; i++) {
            if ((iban.equals(Database.getAllBankAccounts()[i].getIban()))) {
                pinCodeFromList = Database.getAllBankAccounts()[i].getPinCode();
            }

        }
        if (!(pinCodeFromList == pinInputFromUser)) {
            pinCodeDoesNotMatchIban = false;
        }

        return pinCodeDoesNotMatchIban;
    }


    //MENU SCREEN___________________________________________________________
    public static void showMenuScreen(String iban) {

        starLine(printUserInfo(iban));

        System.out.println("\tMenu Options: ");
        System.out.printf("\t\t%-30s[1]\n\t\t%-30s[2]\n\t\t%-30s[3]\n\t\t%-30s[4]\n\n",
                "Incoming transactions ", "Outgoing transactions ", "Make transfer ", "Sign out ");

        int optionUser = KeyboardUtility.askForInt("\tChoose your option: ");
        while (true) {
            if (optionUser == 1) {
                showIncomingScreen();
            } else if (optionUser == 2) {
                showOutgoingScreen();
            } else if (optionUser == 3) {
                showTransferScreen(iban);
            } else if (optionUser == 4) {
                showSignOutScreen();
            } else {
                optionUser = KeyboardUtility.askForInt("\tPlease try again, choose your option: ");
            }
        }
    }

    // print type account based on Iban and balance
    private static String printUserInfo(String iBanToBeChecked) {
        String sentence = null;

        for (int i = 0; i < Database.getAllBankAccounts().length; i++) { // print type account and balance
            if (iBanToBeChecked.equals(Database.getAllBankAccounts()[i].getIban())) { //TODO:extra  print alle acount per klant nummer en toon de saldo
                sentence = String.format("\tWelcome, \n\n\tBankaccountType: %s       Balance: €%,.2f\n\n", Database.getAllBankAccounts()[i].getType(), Database.getAllBankAccounts()[i].getBalance());

            }
        }
        return sentence;
    }


    //INCOMING SCREEN________________________________________________________
    public static void showIncomingScreen() { // created a list incoming and made a method add in class Database
        starLine("\tIncoming transaction history\n");
        System.out.printf("\t\t%-25s %-20s %-20s %-10s %-25s %s\n", "Type", "Sender", "Beneficiary", "Amount", "Date and time", "Communication");
        System.out.print("\t\t____________________________________________________________________________________________________________________________\n");
        if (Database.getIncomingTransactions().length == 0) {
            System.out.println("\t\tNo incoming transactions yet to be shown.");
        } else {
            for (int i = 0; i < Database.getIncomingTransactions().length; i++) {
//TODO: Class Database incoming and outgoing[] Merge --> hier filteren op login van klant
                System.out.print("\t\t" + Database.getIncomingTransactions()[i]);
            }
        }
        goBackToMenuOrNot();
    }



    //OUTGOING SCREEN________________________________________________________
    public static void showOutgoingScreen() {
        starLine("\tOutgoing transaction history\n");

        System.out.printf("\t\t%-25s %-20s %-20s %-10s %-25s %s\n", "Type", "Sender", "Beneficiary", "Amount", "Date and time", "Communication");
        System.out.print("\t\t____________________________________________________________________________________________________________________________\n");
        if (Database.getOutgoingTransactions().length == 0) {
            System.out.println("\t\tNo outgoing transactions yet to be shown.");
        } else {
            for (int i = 0; i < Database.getOutgoingTransactions().length; i++) {
                System.out.print("\t\t" + Database.getOutgoingTransactions()[i]);
            }
        }
        goBackToMenuOrNot();
    }

    public static void showTransferScreen(String senderIbanToBeChecked) {
        starLine("\tTransfers \n");
        System.out.println("\tPlease enter the following information.");

        //TRANSFER SCREEN
        String beneficiaryIban = KeyboardUtility.askForText("\tBeneficiary IBAN: ").toUpperCase(Locale.ROOT);
        senderIban = senderIbanToBeChecked; //incoming parameter

        while (isFoundInAllBankAccounts(beneficiaryIban)) {
            beneficiaryIban = KeyboardUtility.askForText("\tWrong IBAN, please enter your IBAN again: ").toUpperCase(Locale.ROOT);
        }
        System.out.println("\tvalid IBAN! ");
        String beneficiaryName = KeyboardUtility.askForText("\tBeneficiary name: ");
        double beneficiaryAmount = KeyboardUtility.askForDouble("\tAmount: ");

        //Transfer amount could not exceed the balance amount
        while ((retrieveBalance() < beneficiaryAmount)) {
            System.out.printf("\t€%,.2f is not allowed (you need to fund your account in order to transfer)\n", beneficiaryAmount);
            beneficiaryAmount = KeyboardUtility.askForDouble("\tPlease enter an amount under your current balance: " + Database.getAllBankAccounts()[retrieveIndexBasedOnIban(senderIban)].getBalance());
        }
        Database.getAllBankAccounts()[retrieveIndexBasedOnIban(senderIban)].setBalance(Database.getAllBankAccounts()[retrieveIndexBasedOnIban(senderIban)].getBalance() - beneficiaryAmount);
        Database.getAllBankAccounts()[retrieveIndexBasedOnIban(beneficiaryIban)].setBalance(Database.getAllBankAccounts()[retrieveIndexBasedOnIban(beneficiaryIban)].getBalance() + beneficiaryAmount);

        //validation of max 140 charachter
        String communication = KeyboardUtility.askForText("\tCommunication (max 140 characters): ");
        //try qgqin
        while (communication.length() > 140) {
            communication = KeyboardUtility.askForText("\tToo many characters, please enter less than 140 characters");
        }

        //flavor text confirmation
        System.out.printf("\tThe €%,.2f transfer has been successfully transferred to %s to IBAN %s with communication: %s\n", beneficiaryAmount, beneficiaryName, beneficiaryIban, communication);
        System.out.println();
        System.out.printf("\tYour current balance is: €%,.2f \n", Database.getAllBankAccounts()[retrieveIndexBasedOnIban(senderIban)].getBalance());


        String firstName = Database.getClientProfiles()[retrieveIndexBasedOnClientNumber()].getFirstName(); // looking through list getClientProfile[index]
        String lastName = Database.getClientProfiles()[retrieveIndexBasedOnClientNumber()].getLastName();
        //add firstname and lastname in one parameter "sender"

        Transaction outgoingTransaction = new OutgoingTransaction(firstName + " " + lastName, beneficiaryName, beneficiaryAmount, communication, LocalDateTime.now());
        Transaction incomingTransaction = new IncomingTransaction(firstName + " " + lastName, beneficiaryName, beneficiaryAmount, communication, LocalDateTime.now());

        Database.addOutgoingTransfer(outgoingTransaction);//add new object in the correct list
        Database.addIncomingTransfer(incomingTransaction);

        System.out.println("\n\tDo you want to go menu screen or make another transfer or log out? "); // not in method goToMenuOrNot because of extra option
        int option = KeyboardUtility.askForInt("\tMenu[1]\t\t\tAnother transfer[2]\t\t\t Log out[3]\n");
        boolean validOption = false;
        while (!validOption) {
            if (option == 1) {
                validOption = true;
                showMenuScreen(iban);
            } else if (option == 2) {
                break;                  //the extra
            } else if (option == 3) {
                showSignOutScreen();
                validOption = true;
            } else {
                option = KeyboardUtility.askForInt("\tinvalid option, Please try again\n");
            }
        }
    }

    // Checking if the input iban is known in the list of allBankAccounts, returning a true or false
    private static boolean isFoundInAllBankAccounts(String iban) {
        boolean equalIban = false;
        for (int i = 0; i < Database.getAllBankAccounts().length; i++) {
            if ((iban.equals(Database.getAllBankAccounts()[i].getIban()))) {
                equalIban = true;
                break;
            }

        }
        return !equalIban;
    }

    private static int retrieveIndexBasedOnClientNumber() {
        int indexClientNumber = 0;
        int clientNumber = 0;
        for (int i = 0; i < Database.getAllBankAccounts().length; i++) {
            if ((senderIban.equals(Database.getAllBankAccounts()[i].getIban()))) {
                clientNumber = Database.getAllBankAccounts()[i].getClientNumber();
                break;
            }

        }
        for (int i = 0; i < Database.getClientProfiles().length; i++) {
            if ((clientNumber == (ClientProfile.getClientNumber()))) {
                indexClientNumber = i;
                break;
            }

        }
        return indexClientNumber;
    }

    //retrieve balance based on iban on the sender in order to compare the current balance and the transfer amount
    public static double retrieveBalance() {
        double retrieveBalance = 0;
        for (int i = 0; i < Database.getAllBankAccounts().length; i++) {
            if ((senderIban.equals(Database.getAllBankAccounts()[i].getIban()))) {
                retrieveBalance = Database.getAllBankAccounts()[i].getBalance();
                break;
            }
        }
        return retrieveBalance;
    }

    // to know the index of the object in the list
    public static int retrieveIndexBasedOnIban(String ibanToBeChecked) {
        int index = 0;
        for (int i = 0; i < Database.getAllBankAccounts().length; i++) {
            if ((ibanToBeChecked.equals(Database.getAllBankAccounts()[i].getIban()))) {
                index = i;
                break;
            }
        }
        return index;
    }



    //SIGN OUT SCREEN___________________________________________________________
    public static void showSignOutScreen() {
        starLine("\tYou are successfully logged out.\n\tThank you for using our bank app.");
        showLoginScreen();
    }



    //UTILITY____________________________________________________________________
    //menu option method used in several show-screen --code optimisation
    private static void goBackToMenuOrNot() {
        System.out.println("\n\n\tDo you want to go menu screen  or log out? ");
        int option = KeyboardUtility.askForInt("\n\tMenu[1]\t\t\tLog out[2]\n\t");
        boolean validOption = false;
        while (!validOption) {
            if (option == 1) {
                validOption = true;
                showMenuScreen(iban);

            } else if (option == 2) {
                showSignOutScreen();
                validOption = true;

            } else {
                option = KeyboardUtility.askForInt("\tinvalid option, Please try again\n");

            }
        }
    }

    //Flavor decoration
    private static void starLine(String title) {
        System.out.println("\n**************************************************\n");
        System.out.println(title);


    }
}

