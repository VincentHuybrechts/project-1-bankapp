package be.multimedi.project1.thebigbank.transaction;

import java.time.LocalDateTime;


public class IncomingTransaction extends Transaction {
    public IncomingTransaction(String sender, String beneficiary, double transactionAmount, String communication, LocalDateTime dateTime) {
        super(sender, beneficiary, transactionAmount, communication, dateTime, "Incoming Transaction");
        //When a new object is made, the variable "type" in the superclass is automatically filled in as a Outgoing transaction type
    }


}
