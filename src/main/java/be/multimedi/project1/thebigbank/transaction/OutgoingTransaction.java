package be.multimedi.project1.thebigbank.transaction;

import java.time.LocalDateTime;

public class OutgoingTransaction extends Transaction{
    public OutgoingTransaction(String sender, String beneficiary, double transactionAmount, String communication, LocalDateTime dateTime) {
        super(sender, beneficiary, transactionAmount, communication, dateTime, "Outgoing transaction");
        //When a new object is made, the variable "type" in the superclass is automatically filled in as a Outgoing transaction type
    }

}
