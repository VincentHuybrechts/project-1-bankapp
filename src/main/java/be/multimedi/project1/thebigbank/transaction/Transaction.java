package be.multimedi.project1.thebigbank.transaction;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Transaction {

    private final String sender;
    private final String beneficiary;
    private final double transactionAmount;
    private final String communication;
    private final LocalDateTime dateTime;
    private final String type;

    public Transaction(String sender, String beneficiary, double transactionAmount, String communication, LocalDateTime dateTime, String type) {
        this.sender = sender;
        this.beneficiary = beneficiary;
        this.transactionAmount = transactionAmount;
        this.communication = communication;
        this.dateTime = LocalDateTime.now(); //when new object is made, we automatically fill in the current time date
        this.type = type;
    }

    DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);

    @Override
    public String toString() {
        return String.format("%-25s %-20s %-20s %-,10.2f %-25s %s\n", type, sender, beneficiary, transactionAmount, dateTime.format(formatter), communication);
    }


}
